import math


def circle_area(radius=100):
    area = math.pi * radius**2
    return area


def circle_circumference(radius):
    circumference = 2 * math.pi * radius
    return circumference


print(circle_circumference(86))
print(circle_circumference(radius=12))
print(circle_area(radius=34))
print(circle_area())
print(circle_area((lambda y, z: y / z)(6, 2)))
