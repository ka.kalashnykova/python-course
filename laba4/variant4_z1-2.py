import math


def circle_area(radius):
    area = math.pi * radius**2
    return area


r = int(input('Radius: '))
a = circle_area(r)
print('The %s of a circle with %s %5d is %.2f' % ('area', 'radius', r, a))
print('The area of a circle with radius {:^8} is {:>10.2f}'.format(r, a))
print('The number {0:d}  in decimal format, in octal format {0:o},'
      ' in hexadecimal format {0:x} and in exponential format {0:E}'.format(56))
