def line_cross(a1, b1, c1, d1):
    line1min = min(a1, b1)
    line1max = max(a1, b1)
    if line1min <= c1 <= line1max:
        print('Пересекаются')
    elif line1min <= d1 <= line1max:
        print("Пересекаются")
    else:
        print('Не пересекаются')


a = int(input('Введите a: '))
b = int(input('Введите b: '))
c = int(input('Введите c: '))
d = int(input('Введите d: '))
print('Результат: ')
line_cross(a, b, c, d)
